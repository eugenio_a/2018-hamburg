\documentclass[xcolor=dvipsnames]{beamer}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[TS1,T1]{fontenc}

\usetheme[%
matnat,      %% Mathematisch-Naturwissenschaftliche Fakultaet
%nav,         %% Schaltet die Navigationssymbole ein
%latexfonts,  %% Verwendet die latex-beamer-Standardschrift
%colorful,    %% Farbige Balken im infolines-Theme
%squares,     %% Aufzaehlungspunkte rechteckig
%nologo,      %% Kein Logo im Seitenhintergrund
]{UzK}


% -----------------------------------------------------------------------------
\mode<presentation>
\setbeamercovered{invisible}
% -----------------------------------------------------------------------------


% -----------------------------------------------------------------------------
% -- packages
\usepackage{array}
\usepackage{xcolor}
\usepackage{multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{pdfpages}
\usepackage{bibentry}
\usepackage{natbib}
\usepackage{adjustbox}
\usepackage{booktabs}

%% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}

%% pseudocode
%\usepackage{algorithm}
%\usepackage[noend]{algpseudocode}
%\usepackage{algorithmicx}
\usepackage[vlined]{algorithm2e}

%% float placement and inclusion
\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

%% plots
\usepackage{pgfplots}

\usepackage{tikz}
\pgfplotsset{compat=1.9}
\usetikzlibrary{calc}
\usetikzlibrary{backgrounds}
\usetikzlibrary{arrows}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes,decorations.pathmorphing}
\usetikzlibrary{decorations.pathreplacing,arrows,shapes,shapes.geometric}
% -----------------------------------------------------------------------------


% -----------------------------------------------------------------------------
% -- custom commands
%\input include/newcom
% \input include/macros

\newcommand{\balert}[1]{{\color{blue}#1}}
\newcommand{\ralert}[1]{{\color{red}#1}}
\newcommand{\galert}[1]{{\color{ForestGreen}#1}}
\newcommand{\oalert}[1]{{\color{orange}#1}}
\newcommand{\gra}[1]{\texttt{#1}}
\newcommand{\vari}[1]{\texttt{#1}}
\newcommand{\etal}{\textit{et al.\xspace}}

\newcommand\boldblue[1]{\textcolor{blaugrau}{\textbf{#1}}}

\newenvironment{rcases}{% 
  \left.\renewcommand*\lbrace.% 
  \begin{cases}}% 
{\end{cases}\right\rbrace}

\graphicspath{{./images/}}
\makeatletter
\makeatother
% -----------------------------------------------------------------------------


% -----------------------------------------------------------------------------
% -- titlepage
% -----------------------------------------------------------------------------

\title{A NetworKit Tutorial}

\author[Eugenio Angriman]
{Eugenio Angriman}

\institute[Institute of Computer Science]{%
Institute of Computer Science, University of Cologne}

\date{20th February 2018 - PhD Meeting}

\begin{document}
\begin{frame} % [titlepage]
  \maketitle
\end{frame}

\AtBeginSection[]
 {
 \begin{frame}
   \frametitle{Inhalt}
   \tableofcontents[currentsection]
 \end{frame}
 }

\AtBeginSubsection[]
 {
 \begin{frame}
   \frametitle{Inhalt}
   \tableofcontents[currentsection]
 \end{frame}
}



\begin{frame}{Complex Networks}
Heterogeneous datasets with \textbf{common structural characteristics}.\\
\vspace{1cm}
\onslide<2->
Different types on networks
\begin{itemize}
	\item Social
	\item Biological (e.g. proteins interactions)
	\item Infrastructure (e.g. street networks)
	\item Informational (e.g. world wide web)
	\item Collaboration (e.g. imdb, scientific papers)
	\item ... and many more!
\end{itemize}
\end{frame}



\begin{frame}{Complex Networks}
Wide range of applications:
\begin{itemize}
	\item Modern web search
	\item Social media
	\item Social sciences
	\item Medical research
	\item ...
\end{itemize}
\vspace{.2cm}
\onslide<2->
Real-world networks are big!
\begin{itemize}
	\item[$\ast$] Human brain at neuronal scale: $10^{10}$ nodes, $10^{14}$ edges
\end{itemize}
\onslide<3->
\vspace{.5cm}
$\rightarrow$ Fast interactive \textbf{toolkit} required!
\end{frame}



\begin{frame}{NetworKit: main goals and features}
\textbf{Objective}: put efficient network analysis tools in the hands of domain experts
\onslide<2->
\begin{itemize}
	\item Network analysis \textbf{at scale} (big graphs!)
	\item \textbf{Parallelization} (shared memory)
	\item Efficient implementation of several \textbf{graph algorithms}
	\item \textbf{Python} modules $\rightarrow$ Jupyter Notebooks
	\item Constantly updated with SotA algorithms
\end{itemize}
\end{frame}



\begin{frame}{NetworKit: Architecture}
\begin{figure}
\centering
\includegraphics[scale=0.45]{images/NetworKit-Structure.png}
\end{figure}
\end{frame}



\begin{frame}{NetworKit overview}
\begin{itemize}
	\item \textbf{Languages:} C++, Python
	\item \textbf{Parallelism:} OpenMP (shared memory)
	\item \textbf{License:} MIT
	\item \textbf{First release:} March 2013 (version 1.0)
	\item \textbf{Latest release:} December 2017 (version 4.5)
	\item \textbf{Website:} \url{http://network-analysis.info}
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\Huge{Using NetworKit with a Jupyter Notebook}
\end{center}
\end{frame}

\begin{frame}{Reading a graph}
If the graph format is known:
\begin{figure}
\centering
\includegraphics[scale=0.5]{images/JupyterNotebook-read.png}
\end{figure}
\onslide<2->
\textbf{Supported formats}:
\begin{itemize}
	\item KONECT: \url{konect.uni-koblenz.de/networks/}
	\item SNAP: \url{snap.stanford.edu/data/}
	\item METIS
	\item ...
\end{itemize}
\end{frame}



\begin{frame}{Reading a graph}
Handling other graph formats:
\begin{figure}
\centering
\includegraphics[scale=0.5]{images/JupyterNotebook-read-unknown.png}
\end{figure}
\end{frame}



\begin{frame}{Generating a graph}

\begin{figure}
\centering
\includegraphics[scale=0.5]{images/JupyterNotebook-generator.png}
\end{figure}
\onslide<2->
\textbf{Available generators} ('networkit.generators' module):
\begin{itemize}
	\item Erd\H{o}s-R\'enyi: efficient random graphs generator
	\item Barabási-Albert: power law degree distribution
	\item Chung Lu \& Havel-Hakimi: replicate input degree distribution
	\item LFR
	\item ... and many others
\end{itemize}
\end{frame}

\begin{frame}{Writing a graph}

\begin{figure}
\centering
\includegraphics[scale=0.5]{images/GraphWriting.png}
\end{figure}
\onslide<2->
Quick access to \textbf{documentation} via "\textbf{?}"
\begin{figure}
\centering
\includegraphics[scale=0.4]{images/Documentation.png}
\end{figure}
\end{frame}



\begin{frame}{Graph Properties}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.45]{images/GraphProperties.png}
		\end{figure}
	\end{column}
	
	\onslide<2->
	\begin{column}{0.5\textwidth}
		\begin{itemize}
			\item \textbf{Modifiers} (nodes/edges additions or removals, weight updates...)
			\onslide<3->
			\item \textbf{Properties} (number of nodes/edges, degree...)
			\onslide<4->
			\item \textbf{Iterators} over all nodes/edges, also in parallel
			\onslide<5->
			\item \textbf{Random samplers} (select random nodes or edges)
		\end{itemize}
	\end{column}
\end{columns}
\end{frame}



\begin{frame}{NetworKit submodules}
\begin{figure}
\centering
\includegraphics[scale=0.45]{images/Modules.png}
\end{figure}
\onslide<2->
\begin{figure}
\centering
\includegraphics[scale=0.45]{images/ModulesJupyter.png}
\end{figure}
\end{frame}



\begin{frame}{NetworKit analytics}
Three main steps:
\begin{itemize}
	\item \textbf{Create} object instance
	\item \textbf{Run} the algorithm
	\item \textbf{Analyze} the results
\end{itemize}
\end{frame}



\begin{frame}{NetworKit analytics: APSP}
\begin{definition}[All Pairs Shortest Paths]
Given a graph $G = (V, E)$, what is the geodesic distance between all pairs of nodes $u,v \in V, u \neq v$?
\end{definition}

\onslide<2->
Remark:
\begin{itemize}
	\item Quadratic time $\mathcal{O}(n \cdot (n + m))$
	\item Quadratic memory
	\item NetworKit: parallel implementation
\end{itemize}
\end{frame}



\begin{frame}{NetworKit analytics: APSP}
\begin{figure}
	\includegraphics[scale=0.65]{images/APSPExample.png}
\end{figure}
\end{frame}



\begin{frame}{NetworKit analytics: Top-$k$ Closeness}
\begin{definition}[Top-$k$ Closeness]
Given a connected graph $G = (V, E)$ and in integer $k \ge 1$, which are the top-$k$ nodes with highest closeness centrality?\\
\vspace{.5cm}
Closeness centrality of a node $v \in V$:
\[
	c(v) := \frac{n-1}{\sum_{w \in V}{d(v, w)}}
\]
\end{definition}

\onslide<2->
\begin{itemize}
	\item Quadratic time (need to solve APSP)
	\item NetworKit: near-linear time on real-world networks
\end{itemize}
\end{frame}



\begin{frame}{NetworKit analytics: Top-$5$ Closeness example}
\begin{figure}
	\includegraphics[scale=0.65]{images/TopClosenessExample.png}
\end{figure}
\end{frame}



\begin{frame}{Running time Comparison}
\begin{figure}
	\centering
	\includegraphics[scale=0.18]{images/Comparison.png}
	\caption{Running times for some analysis kernels for NewtworkX, graph-tool and NetworKit. \textcolor{RoyalBlue}{\scriptsize{[Staudt et al, 2014]}}\\
	$\circ$: feature is missing\\
	$\bowtie$: running time $> 2$h\\
	$\dagger$: runs out of memory}
\end{figure}
\end{frame}



\begin{frame}{Graph Drawing with Gephi}
\begin{columns}
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{images/Gephi.png}
	\end{figure}
	\onslide<2->
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.17]{images/GraphGrayTransparent.png}
	\end{figure}
\end{columns}

Not very useful to draw an all-gray graph...
\end{frame}



\begin{frame}{Graph Drawing with Gephi: Core Decomposition}
\begin{columns}
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.35]{images/kCoresGephi.png}
	\end{figure}
	\onslide<2->
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.2]{images/Graph_Color_Transparent.png}
		\caption*{Each color represents a core of the graph.}
	\end{figure}
\end{columns}
\end{frame}



\begin{frame}{Graph Drawing: Closeness Centrality}
\begin{columns}
	\column{.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=0.35]{images/GephiClosenessPython.png}
		\end{figure}
	\onslide<2->
	\column{.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=0.15]{images/GephiClosenessTransparent.png}
			\caption*{Red: low closeness\\
			Blue: high closeness}
		\end{figure}
\end{columns}
\end{frame}



\begin{frame}{NetworKit profiling}
\begin{itemize}
	\item Provides a detailed profile of the network
	\item Includes several sections:
	\begin{itemize}
		\item Global properties
		\item Node centrality and partition overview
		\item Detailed view of centrality distributions
		\item Centrality correlations
		\item Detailed view of partitions
	\end{itemize}
\end{itemize}
\end{frame}



\begin{frame}{NK profiling: How to and Global properties}
\begin{columns}
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.41]{images/ProfilingTool.png}
	\end{figure}
	\onslide<2->
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{images/GlobalProperties.png}
	\end{figure}
\end{columns}
\end{frame}



\begin{frame}{NK profiling: Centrality and Partitions overview}
\begin{figure}
	\centering
	\includegraphics[scale=0.31]{images/CentralityOverview.png}
\end{figure}
\end{frame}



\begin{frame}{NK profiling: Detailed view example}
\begin{figure}
	\centering
	\includegraphics[scale=0.32]{images/Detail.png}
\end{figure}
\end{frame}



\begin{frame}{NK profiling: Correlations}
\begin{figure}
	\centering
	\includegraphics[scale=0.4]{images/Correlation.png}
\end{figure}
\begin{columns}
	\column{.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=0.5]{images/Corr1.png}
		\end{figure}
	\column{.5\textwidth}
	\begin{figure}
		\centering
		\includegraphics[scale=0.5]{images/Corr2.png}
	\end{figure}
\end{columns}
\end{frame}



\begin{frame}
\begin{center}
	\Huge{Working with the  C++ core}
\end{center}
\end{frame}



\begin{frame}{NetworKit Graph data structure}
\begin{columns}
	
		\column{.33\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=0.4]{images/AdjList.png}
		\end{figure}
	
		\column{.66\textwidth}
		\begin{itemize}
			\item Data structure: adjacency array		
			\item Basic container: std::vector
			\item Memory requirement: $\mathcal{O}(n + m)$
		\end{itemize}
		\onslide<2->
		\begin{figure}
			\centering
			\includegraphics[scale=0.35]{images/ForNodes.png}
		\end{figure}
		\onslide<3->
		\begin{figure}
			\centering
			\includegraphics[scale=0.24]{images/ParallelForNodes.png}
		\end{figure}

\end{columns}
\end{frame}



\begin{frame}{Developing new Classes}
C++ classes organization:
\begin{itemize}
	\item \url{networkit/cpp/<module name>/<your class>}\\
	(Same modules as in Python)
	\item Google Tests: \url{networkit/cpp/<module name>/test}
	\item \textbf{Always} test your classes!
\end{itemize}
\vspace{.5cm}
\onslide<2->
Exposing to Python:
\begin{itemize}
	\item Edit file \url{networkit/_NetworKit.pyx}
	\item Add your class to a \url{*.py} file in the \url{networkit} directory
	\item More detailed info at:
\end{itemize}
\center{\url{http://network-analysis.info/api/DevGuide.html}}
\end{frame}



\begin{frame}{Recent NetworKit updates}
\begin{itemize}
	\item LAMG solver implementation
	\item Electrical Closeness
	\item Dynamic Algorithms
	\item Graph Generators
	\item Group Closeness
\end{itemize}
\end{frame}


\begin{frame}{LAMG Solver Implementation}
\begin{itemize}
	\item Solver for \emph{Laplacian linear systems}:
	\[Lx = b, \textnormal{ where } L:= D - A\]	\onslide<2->
	\item Useful to solve several network analysis problems:
		\begin{columns}
			\column{.4\textwidth}
			\begin{itemize}
				\item[$\ast$] Graph partitioning
				\item[$\ast$] Approx. max flow
				\item[$\ast$] ...
			\end{itemize}
			\column{.3\textwidth}
			\begin{itemize}
				\item[$\ast$] Sparsification
				\item[$\ast$] Graph drawing
			\end{itemize}
		\end{columns}
	\onslide<3->
	\vspace{.3cm}
	\item LAMG \textcolor{RoyalBlue}{\scriptsize{[Livne and Brandt, 2012]}}:
	\begin{itemize}
		\item Algebraic multigrid
		\item Iteratively solve coarser system
		\item Prolong solutions to original systems
		\item Designed for complex networks
	\end{itemize}

	\vspace{.2cm}	
	\item Empirical running time for a linear system: $\mathcal{O}\left( m \cdot \ln{1/\epsilon} \right)$
\end{itemize}
\end{frame}



\begin{frame}{LAMG Solver Implementation}
\begin{itemize}
	\item NetworKit: first network analysis toolkit providing a fast Laplacian solver implementation based on multigrid
	\onslide<2->
	\vspace{.2cm}
	\item New features:
	\begin{itemize}
		\item[$\ast$] Commute-time distance ($u,v \in V$):
			\[
				d_{CT}(u,v) := H(u,v) + H(v,u)
			\]
		\onslide<3->
		\item[$\ast$] Spanning edge centrality
			\[
				c_{SE}(e) :=  \frac{ST(G,e)}{ST(G)}
			\]
		\onslide<4->
		\item[$\ast$] Electrical closeness
			\[
				C_{EC} := \frac{n - 1}{\sum_{v \in V}{d_{CT}(u,v)}}
			\]
	\end{itemize}
\end{itemize}
\end{frame}



\begin{frame}{Electrical vs Shortest-path Closeness}
\textbf{Differentiation among different nodes }\textcolor{RoyalBlue}{\scriptsize{[Bergamini et al., CSC 2016]}}\\
\begin{itemize}
	\item Real-world complex networks have small diameter
	\item Many nodes have similar shortest-path closeness
\end{itemize}
\begin{figure}
	\centering
	\includegraphics[scale=0.45]{images/CurrentFlow.png}
\end{figure}
\end{frame}



\begin{frame}{Electrical vs Shortest-path Closeness}
\textbf{Resilient to noise }\textcolor{RoyalBlue}{\scriptsize{[Bergamini et al., CSC 2016]}}\\
Experiment: add new edges and recompute centrality ranking.
\begin{figure}
\centering
\includegraphics[scale=0.4]{images/Spearmann.png}
\end{figure}
\end{frame}



\begin{frame}{Evolving networks}
Many real-world networks \textbf{change} over time
\begin{itemize}
	\item A new users registers in a social network (node addition)
	\item A friendship is terminated (edge removal)
	\item A new friendship is made (edge addition)
	\item ...
\end{itemize}
\onslide<2->
\textbf{Graph update}: edge/node addition/removal\\
\onslide<3->
\vspace{.3cm}
\textbf{Dynamic algorithms}:
\begin{itemize}
	\item Compute a metric on the initial graph (e.g. Betweenness)
	\item Update the metric after one or more graph updates
	\item Re-use available information to avoid unnecessary work
\end{itemize}
\end{frame}



\begin{frame}{Dynamic algorithms with NetworKit}
\begin{itemize}
	\item Common interface (base class: \url{DynAlgorithm}):
	\begin{itemize}
		\item \textbf{run()} method to run on the static algorithm
		\item \textbf{update()} to launch an update (requires a GraphEvent instance)
		\item \textbf{updateBatch()} to update after multiple graph events
	\end{itemize}
	\onslide<2->
	\vspace{.3cm}
	\item Dynamic algorithms for:
	\begin{itemize}
		\item Betweenness centrality (exact and approximate)
		\item APSP and SSSP (BFS and Dijkstra)
		\item (Weakly) Connected components
		\item Harmonic closeness centrality
	\end{itemize}
\end{itemize}
\end{frame}



\begin{frame}{Dynamic betweenness centrality}
\begin{columns}
	\column{.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=.35]{images/DynBetweenness1.png}
		\end{figure}
	\onslide<2->
	\column{.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[scale=.35]{images/DynBetweenness2.png}
		\end{figure}
\end{columns}
\end{frame}



\begin{frame}{Dynamic weakly connected components}
\begin{figure}
	\centering
	\includegraphics[scale=.35]{images/DynComponents.png}
\end{figure}
\end{frame}



\begin{frame}{Group Closeness}
\begin{definition}[Group Closeness]
Given a graph $G = (V, E)$ and a non-empty subset $S \subseteq V$, the closeness centrality of $S$ is defined as:
	\[
		c(S) := \frac{n - |S|}{\sum_{v \notin S}{d(S, v)}}
	\]
\end{definition}
\end{frame}



\begin{frame}{Group Closeness Maximization problem}
\begin{definition}[Group Closeness Maximization problem]
Given a graph $G = (V, E)$ and an integer $1 \le k \le |V|$, find:
	\[
		S^\star  = \arg\max_{S \subseteq V}{\{c(S) : |S| = k\}}			
	\]
\end{definition}
\onslide<2->
\begin{itemize}
	\item NP-hard problem
	\item \texttt{Greedy++}: fast $(1 - 1/e)$-approximation algorithm
	\\\textcolor{RoyalBlue}{\scriptsize{[Bergamini et al., ALENEX 2018]}}
\end{itemize}
\end{frame}



\begin{frame}{Group Closeness: quick example}
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{images/GroupCloseness.png}
\end{figure}
\end{frame}



\begin{frame}{Summary}
\begin{itemize}
	\item NetworKit: \textbf{toolkit} for network analysis \textbf{at scale}
	\item Main techniques: shared-memory parallelism, approximation, fast heuristics for real-world networks, efficient data structures
	\item Presented features:
	\begin{itemize}
		\item Graph I/O and generators
		\item Graph drawing with Gephi
		\item Profile tool
		\item Dynamic algorithms
	\end{itemize}
\end{itemize}
\end{frame}



\begin{frame}{Participation, Info \& Support}
\textbf{Participation}:
\begin{itemize}
	\item Algorithm development
	\item Network analysis with NetworKit
	\item Teaching graph algorithms with NetworKit hands-on tool
\end{itemize}
\onslide<2->
\textbf{Resources}:
\begin{itemize}
	\item \textbf{Website} \url{http://network-analysis.info}
	\item \textbf{GitHub link}: \url{https://github.com/kit-parco/networkit}
	\item \textbf{Technical report}: \url{http://arxiv.org/abs/1403.3005}
	\item \textbf{Updated ICPP CD paper}: \url{http://arxiv.org/abs/1304.4453}
	\item Package documentation
	\item \textbf{E-mail list}: \url{networkit@ira.uni-karlsruhe.de}
\end{itemize}
\end{frame}



\begin{frame}
\Huge{\centerline{Thank you for your attention!}}
\end{frame}
\end{document}
